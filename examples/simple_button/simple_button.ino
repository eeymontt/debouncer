/*******************************************************************************
 * Example sketch using the "Debouncer" library.
 * 
 * Ethan Eymontt
 * 23 October 2022
 *
 *******************************************************************************
 * Schematic:
 * 
 * One side of a normally open pushbutton is pulled up to VCC via a resistor,
 * and the other side is connected to digital Pin 3 on the Arduino board.
 ******************************************************************************/

#include "debouncer.h"

#define BUTTON_PIN  3

void printPressed(uint8_t n);

using namespace eymonttdev;
auto d = Debouncer();

void setup()
{
    Serial.begin(9600);
    pinMode(BUTTON_PIN, INPUT); // <-- initialize GPIO

    delay(1000);

    /* When pressed, button pulls pin low. */
    d.register_cb(Debouncer::EdgeType::Fall, printPressed);
}

void loop()
{
    d.update();
    delay(10);
}

void printPressed(uint8_t n) { Serial.println("Button pressed!"); }