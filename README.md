# Debouncer

The **Debouncer** library is used for debouncing pin inputs that are influenced by less-than-ideal hardware circuitry elements, such as pushbuttons. Currently it only works on digital pins (i.e. those with a distinct *High* and *Low* state).

## Namespace

The `Debouncer` library resides within the `eymonttdev` namespace.

## Configuration

`Debouncer` objects can be configured for a variety of use cases; they can also be re-configured or reset as program needs change without the need to create a new class instance.

### Mode

The debouncer mode determines the method through which inputs are considered significant. The more responsive mode (`Debouncer::DurationFrom::Trigger`) recognizes input changes with minimal delay and is good for when you expect many changes to the input. The more stable mode (`Debouncer::DurationFrom::Stable`) recognizes input changes in a similar manner, but prevents states from changing too quickly.

### ___1

### Platform

This tool is cross-platform which means that it is up to the user to configure the library for use with specific microcontroller hardware. If you intend to use the Arduino framework, you're in luck; it's the default mode and you do not need to do anything special. Otherwise, provide a macro definition and two functions with the following capabilities:

* Read the state of the target GPIO
* Retrieve current system time

For an example of this, see section [General Usage: Configure Hardware Layer](#configure-hardware-layer).

### Callbacks

On input changes, callback functions can be executed automatically by the `Debouncer`.

They can be assigned to any action, but only one function can be assigned per action:

* `EdgeType::Fall` - Execute callback on a falling edge
* `EdgeType::Rise` - Execute callback on a rising edge
* `EdgeType::Change` - Execute callback on either a falling or rising edge

## General Usage

### Create a basic `Debouncer` object

```c++
#include "debouncer.h"

int main (int argc, char *argv[])
{
    auto d = eymonttdev::Debouncer();

    while (true)
    {
        d.update();
    }
}
```

The `update` function must be called every program loop or as often as possible.

### ___2

### Assign Callbacks

Callback functions can be assigned to the `Debouncer` instance either by reference to a function defined in the main code, or with a lambda function. Callbacks are assigned through the `register_cb` function.

```c++
// external function
void myFunction(uint8_t n) { cout << "Falling edge detected on pin: " << int(n) << endl; }
d.register_cb(Debouncer::EdgeType::Fall, myFunction);

// lambda function
d.register_cb(Debouncer::EdgeType::Rise, [](uint8_t n) -> void { cout << "Pin " << int(n) << " is HIGH" << endl; })
```

Callback functions must have the signature: `void (*debounce_fn_cb)(uint8_t)`, as shown above.

You can reassign callbacks by calling the `register_cb` function again with the same `EdgeType`. You can also de-register callbacks with the `deregister_cb` function.

```c++
// do nothing on a rising edge
d.deregister_cb(Debouncer::EdgeType::Fall);

// de-register all callbacks
d.deregister_cb();
```

### ___3

### Manual Interface

The `Debouncer` object can also be used in a less automated manner (i.e., if you do not want to use the clalback functionality).

Simple check the state of the input after updating the `Debouncer`.

```c++
//...
```

...

FUTURE WORK TO INCLUDE A DEBOUNCER COUNTER, such as with:
[LINK](https://github.com/wkoch/Debounce/blob/master/Debounce.h)

with the ability to get count (increase per thing "need to set what a press is, if high or low"), reset count!

Also functionality about how long a button has been pressed for... that's useful information... as with:
[LINK](https://github.com/Mokolea/InputDebounce/blob/master/src/InputDebounce.cpp)

### Configure Hardware Layer

By default, this tool leverages the Arduino framework. After creating a `Debouncer`, you can just call `update()` and be good to go.

```c++
#include <Arduino.h>

// use `millis()` and `digitalRead()` functions automatically
#include "debouncer.h"

//...
```

To make this program compatible with a microcontroller that does not support a *duino framework, you need to define `DEBOUNCE_CUSTOM_FRAMEWORK` in your main code prior to including the library header, as shown below.

Alternately, to use an different framework you must provide a custom implementation. This requires defining two functions with the syntax below:

```c++
/* Return run time of program as milliseconds since start. */
uint32_t sys_time_ms();

/* Return GPIO state. 0 = Low, 1 = High */
uint8_t gpio_digital_state(uint8_t pin);
```

As in *my_program.cpp*:

```c++
#define DEBOUNCE_CUSTOM_FRAMEWORK
#include "debouncer.h"

uint32_t sys_time_ms() { /* return ... */ }
uint8_t gpio_digital_state(uint8_t pin) { /* return ... */ }

int main()
{
    //...
}
```

In all cases, the initialization of the GPIO pin itself is the responsibility of the user.

*E.g.* Using the Arduino framework

```c++
#include <Arduino.h>    // use Arduino framework
#include "debouncer.h"

#define BUTTON_PIN  3

void printPressed(uint8_t n);

using namespace eymonttdev;
auto d = Debouncer();

void setup()
{
    Serial.begin(9600);
    pinMode(BUTTON_PIN, INPUT); // <-- initialize GPIO

    /* When pressed, button pulls pin low. */
    d.register_cb(Debouncer::EdgeType::Fall, printPressed);
}

void loop()
{
    d.update();
    delay(10);
}

void printPressed(uint8_t n) { Serial.println("Button pressed!"); }
```
