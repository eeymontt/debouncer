#include "debouncer.h"

#ifndef DEBOUNCE_CUSTOM_FRAMEWORK
    #include <Arduino.h>
#endif

namespace eymonttdev {

void Debouncer::update()
{
    uint32_t time_curr_ms;
    uint8_t pin_state;

    #ifndef DEBOUNCE_CUSTOM_FRAMEWORK
        time_curr_ms = millis();
        pin_state = digitalRead(pin_target_);
    #else
        time_curr_ms = sys_time_ms();
        pin_state = gpio_digital_state(pin_target_);
    #endif /* DEBOUNCE_CUSTOM_FRAMEWORK */

    update(time_curr_ms, pin_state);
}

void Debouncer::update(uint32_t time_curr_ms, uint8_t pin_state)
{
    ActiveType current_state_ = (bool)pin_state ? ActiveType::High : ActiveType::Low;

    edge_detected_ = false;
    is_rising_ = false;
    is_falling_ = false;

    // definite change in output -> potential change in stable state
    if (current_state_ != previous_state_)
    {
        previous_state_ = current_state_;
        if (is_in_stable_state_)
        {
            is_in_stable_state_ = false;

            // record time of instability started
            time_last_trigger_start_ = time_curr_ms;
        }
        // record most recent time of instability
        time_last_trigger_end_ = time_curr_ms;
    }
    else
    {
        // when no change in output, let's resolve stability
        if (!is_in_stable_state_)
        {
            // debounce based on check condition
            uint32_t time_last_trigger = (mode_ == DurationFrom::Stable)
                ? time_last_trigger_end_
                : time_last_trigger_start_;
            
            // duration based on pin state
            uint32_t time_threshold_ms = (stable_state_ == current_state_)
                ? time_threshold_on_ms_
                : time_threshold_off_ms_;
            
            if ((time_curr_ms - time_last_trigger) > time_threshold_ms)
            {
                if (stable_state_ != current_state_)
                {
                    // state change
                    stable_state_ = current_state_;
                    is_in_stable_state_ = true;
                    edge_detected_ = true;

                    //time_last_trigger_end_ = 0xFFFFFFFF;
                    //time_last_trigger_start_ = 0xFFFFFFFF;

                    // execute callbacks
                    if (stable_state_ == ActiveType::Low)
                    {
                        // became low
                        is_falling_ = true;
                        if (fn_edge_fall_ptr) fn_edge_fall_ptr(pin_target_);
                    }
                    else
                    {
                        // become high
                        is_rising_ = true;
                        if (fn_edge_rise_ptr) fn_edge_rise_ptr(pin_target_);
                    }

                    // changed to sthg else regardless
                    if (fn_edge_change_ptr) fn_edge_change_ptr(pin_target_);
                }
            }
        }
    }
}

void Debouncer::register_cb(const EdgeType edge, const debounce_fn_cb& func)
{
    switch (edge)
    {
        case EdgeType::Rise:
        {
            fn_edge_rise_ptr = func;
            break;
        }
        case EdgeType::Fall:
        {
            fn_edge_fall_ptr = func;
            break;
        }
        case EdgeType::Change:
        {
            fn_edge_change_ptr = func;
            break;
        }
    }
}

void Debouncer::deregister_cb(const EdgeType edge)
{
    switch (edge)
    {
        case EdgeType::Rise:
        {
            fn_edge_rise_ptr = nullptr;
            break;
        }
        case EdgeType::Fall:
        {
            fn_edge_fall_ptr = nullptr;
            break;
        }
        case EdgeType::Change:
        {
            fn_edge_change_ptr = nullptr;
            break;
        }
    }
}

} // namespace eymonttdev