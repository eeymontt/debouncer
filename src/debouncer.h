/** @file */

#ifndef EYMDEV__DEBOUNCER_WITH_CALLBACK__H
#define EYMDEV__DEBOUNCER_WITH_CALLBACK__H

#include <stdint.h>

#define DEBOUNCE_DEFAULT_PIN_ASSIGN     (0xFF)
#define DEBOUNCE_DEFAULT_DELAY_MS       (50)

#ifdef DEBOUNCE_CUSTOM_FRAMEWORK
    extern uint32_t sys_time_ms();
    extern uint8_t gpio_digital_state(uint8_t pin);
#endif

namespace eymonttdev {

    typedef void (*debounce_fn_cb)(uint8_t);

    class Debouncer
    {
        public:
            /** Debounce mode. */
            enum class DurationFrom
            {
                /**
                 * For a change of state to be recognized, the previous input
                 * state must exist for some minimum duration prior.
                 */
                Stable,
                /**
                 * For a change of state to be recognized, the new state must 
                 * persist for some minimum duration.
                 */
                Trigger     
            };

            /** Input state. */
            enum class ActiveType
            {
                Low,
                High
            };

            /** Trigger type. */
            enum class EdgeType
            {
                Fall,   /**< Change in state from High to Low. */
                Rise,   /**< Change in state from Low to High. */
                Change  /**< Any type of change in state. */
            };            

            /**
             * @brief Create a Debouncer object.
             * 
             * @param[in] pin I/O target.
             * @param[in] duration Time of significance for state change, in [ms].
             * @param[in] active_state Current state of target I/O.
             * @param[in] mode Method of debouncing to use.
             */
            Debouncer(
                uint8_t pin = DEBOUNCE_DEFAULT_PIN_ASSIGN,
                uint32_t duration = DEBOUNCE_DEFAULT_DELAY_MS,
                const ActiveType active_state = ActiveType::Low,
                const DurationFrom mode = DurationFrom::Stable)
            : pin_target_(pin)
            , time_threshold_on_ms_(duration)
            , time_threshold_off_ms_(duration)
            , stable_state_(active_state)
            , previous_state_(active_state)
            , mode_(mode)
            , time_last_trigger_start_(0)
            , time_last_trigger_end_(0) {}

            /**
             * @brief Run the debouncer.
             * @param[in] time_curr_ms Current program running time, [ms].
             * @param[in] pin_state Digital read state on GPIO (1 or 0).
            */
            void update(uint32_t time_curr_ms, uint8_t pin_state);
            void update();

            /* For manual usage of library */

            bool edge() const { return edge_detected_; }
            bool changed() const { return edge(); }
            bool rising() const { return is_rising_ ; }
            bool falling() const { return is_falling_; }
            ActiveType get_state() const { return stable_state_; }

            /* Assign/deassign callbacks to state changes */

            void register_cb(const EdgeType edge, const debounce_fn_cb& func);
            void deregister_cb(const EdgeType edge);
        
        private:
            uint8_t pin_target_;

            uint32_t time_last_trigger_start_;
            uint32_t time_last_trigger_end_;

            uint32_t time_threshold_on_ms_;
            uint32_t time_threshold_off_ms_;

            ActiveType stable_state_;
            ActiveType previous_state_;

            DurationFrom mode_ = DurationFrom::Stable;

            bool edge_detected_ = false;
            bool is_rising_ = false;
            bool is_falling_ = false;

            bool is_in_stable_state_ = false;

            // callback functions
            debounce_fn_cb fn_edge_fall_ptr = nullptr;
            debounce_fn_cb fn_edge_rise_ptr = nullptr;
            debounce_fn_cb fn_edge_change_ptr = nullptr;
    };

} // namespace eymonttdev

#endif /* EYMDEV__DEBOUNCER_WITH_CALLBACK__H */ 